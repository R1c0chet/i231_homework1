public class Sheep {

    enum Animal {sheep, goat}

    public static void main(String[] param) {
//         for debugging
    }

    public static void reorder (Animal[]animals) {

        Animal[] sortedAnimals = new Animal[animals.length];

        int g = 0;
        int s = animals.length - 1;

        for (int i = 0; i < animals.length; i++) {
            switch (animals[i]) {
                case sheep:
                    sortedAnimals[s] = animals[i];
                    s--;
                    break;
                case goat:
                    sortedAnimals[g] = animals[i];
                    g++;
                    break;
            }
        }

        for (int i = 0; i < animals.length; i++) {
            animals[i] = sortedAnimals[i];
        }
    }
}


